/* Example of implementing related types in C++ */
#include <iostream.h>
#include <string.h>

const int MAXLENGTH = 100;

class Employee
{
public:
	Employee();
	Employee (const char *nm);
	char *getName() const {return name;}
private:
	char name[30];
};
 
Employee::Employee (const char *nm)
{
	strcpy(name, nm);
}

class WageEmployee : public Employee
{
public:
	WageEmployee ();
	WageEmployee (const char *nm);
	void setWage (float wg) {wage = wg;}
	void setHours (float hrs) {hours = hrs;} 
	void WageEmployee::printName() const; 
	float computePay() const { return wage * hours;}
private:
	float wage;
	float hours;
};

WageEmployee::WageEmployee (const char *nm)
	:Employee (nm)
{
	wage = 0.0;
	hours = 0.0;
}	

WageEmployee::WageEmployee ()
	:Employee (" ")
{
	wage = 0.0;
	hours = 0.0;
}	


void WageEmployee::printName() const
{
	cout << "Worker's name: " << getName() << '\n'; // Call Employee::getName 
}
 
class SalesPerson : public WageEmployee
{
public:
	SalesPerson (const char *nm);
	void setCommision (float comm) {commision = comm;}
	void setSales (float sales) {salesMade = sales;}     
	float computePay() const 
	{ return WageEmployee::computePay() + commision * salesMade;}
private:
	float commision;
	float salesMade;
};

SalesPerson::SalesPerson (const char *nm)
	:WageEmployee (nm)
{               
	commision = 0.0;
	salesMade = 0.0;
}	

class Manager : public Employee
{
public:
	Manager (const char *nm);
	void setSalary (float salary) {weeklySalary = salary;}
	float computePay() const { return weeklySalary;}
private:
	float weeklySalary;
};

Manager::Manager (const char *nm)
	: Employee (nm)
{                   
	weeklySalary = 0.0;
}


class EmployeeList
{
friend class EmployeeIter;
public:
	EmployeeList();
	int add (Employee *newEmp);
private:
	Employee *aray[MAXLENGTH];
	int firstEmpty;			// First unused element
};


EmployeeList::EmployeeList()
{
	firstEmpty = 0;
}

int EmployeeList::add(Employee *newEmp)
{
	if (firstEmpty+1 < MAXLENGTH)
	{
		aray[firstEmpty] = newEmp;
		firstEmpty++;
		return 0;
	}
	else
		return 1; 
}

class EmployeeIter
{
public:
	EmployeeIter (EmployeeList &m);
	Employee *getFirst();
	Employee *getLast();
	Employee *getNext();
	Employee *getPrev();
private:
	EmployeeList *const mine;		// Pointer to a EmployeeList object
	int currIndex;
};

EmployeeIter::EmployeeIter (EmployeeList &m)
	: mine ( &m )				// Initialize the constant member
{
	currIndex = 0; 
}
	
Employee *EmployeeIter::getFirst()
{
	currIndex = 0;
	return (mine->aray[currIndex]);
}

Employee *EmployeeIter::getLast()
{
	currIndex = mine->firstEmpty -1;
	return (mine->aray[currIndex]);
}	

Employee *EmployeeIter::getNext()
{
	if (currIndex < mine->firstEmpty - 1)		
    {
    	currIndex++;
    	return (mine->aray[currIndex]);
    }
    else
    	return 0;
}


Employee *EmployeeIter::getPrev()
{
	if (currIndex > 0)
	{
		currIndex--;
		return (mine->aray[currIndex]);
	}
	else
		return 0;
}

void printNames (EmployeeList &dept)
{
	int count = 0;
	Employee *person;
	EmployeeIter anIter (dept);		// Iterator object
	 
	person = anIter.getFirst();
	cout << count << ' ' << person->getName() << '\n';
	 
	while ( person = anIter.getNext() )	
	{
		count++;
		cout << count << ' '
			 << person->getName() << '\n';
	}
}


main()
{

	EmployeeList myDept;
	WageEmployee *wagePtr;
	SalesPerson *salePtr;
	Manager *mgrPtr;
	
	// Allocate new objects
	wagePtr = new WageEmployee ("Bill Shapiro");
	salePtr = new SalesPerson ("John Smith");
	mgrPtr = new Manager ("Mary Brown");
	
	// Add them to the list
	myDept.add (wagePtr);
	myDept.add (salePtr);
	myDept.add (mgrPtr);
	
	printNames (myDept);
		
	return 0;	                  
	
}
 
 
