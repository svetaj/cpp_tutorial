// BAD TECHNIQUE: modifying a parameter trough a reference
#include <iostream.h>

void print( int &parm )
{
        cout << parm;
        parm = 0;
};

void main()
{
        int a = 5;

        print( a );     // Parameter is modified;
                        // unexpecter side effect
}

