/* Example of implementing related types in C++ */
#include <iostream.h>
#include <string.h>

class Employee
{
public:
	Employee();
	Employee (const char *nm);
        char *getName() const { return (char *)name; }  
private:
	char name[30];
};
 
Employee::Employee( const char *nm)
{
	strcpy(name, nm);
}

class WageEmployee : public Employee
{
public:
	WageEmployee (const char *nm);
	void setWage (float wg) {wage = wg;}
	void setHours (float hrs) {hours = hrs;} 
	void WageEmployee::printName() const; 
	float computePay() const { return wage * hours;}
private:
	float wage;
	float hours;
};

WageEmployee::WageEmployee (const char *nm)
	:Employee (nm)
{
	wage = 0.0;
	hours = 0.0;
}	

void WageEmployee::printName() const
{
	cout << "Worker's name: " << getName() << '\n'; // Call Employee::getName 
}
 
class SalesPerson : public WageEmployee
{
public:
	SalesPerson (const char *nm);
	void setCommision (float comm) {commision = comm;}
	void setSales (float sales) {salesMade = sales;}     
	float SalesPerson::computePay() const 
	{ return WageEmployee::computePay() + commision * salesMade;}
private:
	float commision;
	float salesMade;
};

SalesPerson::SalesPerson (const char *nm)
	:WageEmployee (nm)
{               
	commision = 0.0;
	salesMade = 0.0;
}	

class Manager : public Employee
{
public:
	Manager (const char *nm);
	void setSalary (float salary) {weeklySalary = salary;}
	float Manager::computePay() const { return weeklySalary;}
private:
	float weeklySalary;
};

Manager::Manager (const char *nm)
	: Employee (nm)
{                   
	weeklySalary = 0.0;
}



main()
{
	WageEmployee aWorker ("Bill Shapiro");
	char *str;
	
	aWorker.setHours (40.0);		// Call WageEmployee::setHours
	str = aWorker.getName();		// Call Employee::getName 
	cout << "Worker name = " << str << '\n';   
	aWorker.printName();
	
	SalesPerson aSeller ("John Smith");
	
	aSeller.setHours (40.0);
	aSeller.setWage (6.0);
	aSeller.setCommision (0.05);
	aSeller.setSales (2000.0);
        aSeller.printName();
	
	// Call SalesPerson::computePay
	cout << "Seller salary: " << (int) aSeller.computePay() << '\n';	
	cout << "Seller base salary: " << (int) aSeller.WageEmployee::computePay() << '\n';
	
	return 0;	
}
 
 
