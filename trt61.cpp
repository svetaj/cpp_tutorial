// Using header and source files

#include <iostream.h>
#include <string.h>
#include "date.h"


void main()
{
        Date myDate;             // Declare a date without arguments
        myDate.display();
        Date yourDate( 12, 25, 1990 );
        yourDate.display();
	
        myDate.setMonth( 3 );   // Set values for myDate
        myDate.setDay( 12 );
        myDate.setYear( 1985 );
        myDate.display();
}
