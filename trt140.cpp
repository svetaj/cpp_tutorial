// Overloading the + operator
#include <stdlib.h>
#include <math.h>
#include <iostream.h>

class Fraction
{
public:
	Fraction();
	Fraction (long num, long den);
	void display() const;
	Fraction operator+ (const Fraction &second) const;
	Fraction Fraction::operator+ (long second) const;
private:
	static long gcf (long first, long second);
	long numerator, denominator;
};

// ------------- Default constructor
Fraction::Fraction()
{
	numerator = 0;
	denominator = 1;
}

// ------------- Constructor
Fraction::Fraction (long num, long den)
{
	int factor;
	
	if (den == 0)
		den = 1;
	numerator = num;
	denominator = den;
	if (den < 0)
	{
		numerator = -numerator;
		denominator = -denominator;
	}
	factor = (int) gcf (num, den);
	if (factor > 1)
	{
		numerator /= factor;
		denominator /= factor;
	}
}

// ------------- Function to print a Fraction
void Fraction::display() const
{
	cout << numerator << '/' << denominator; 
}

// ------------- Overloaded + operator
Fraction Fraction:: operator+ (const Fraction &second) const
{
	long factor, mult1, mult2;
	
	factor = gcf (denominator, second.denominator);
	mult1 = denominator / factor;
	mult2 = second.denominator / factor;
	
	return Fraction (numerator * mult2 + second.numerator * mult1, 
					 denominator * mult2);
}

// ------------ Fraction + integer
Fraction Fraction::operator+ (long second) const
{
	return Fraction (numerator + second * denominator, denominator);
}

// -------------- Greatest common factor
// computed using iterative version of Euclid's algorithm
long Fraction::gcf (long first, long second)
{
	int temp;
	
	first = labs (first);
	second = labs (second);
	
	while (second > 0)
	{
		temp = (int) (first % second);
		first = second;
		second = (long) temp;
	}
	
	return first;
}

void main()
{
	Fraction a, b(23, 11), c(2,3);
	
	a = b + c; 			// as b.operator+ (c)
	
	
	a.display();
	cout << " = ";
	b.display();
	cout << " + ";
	c.display();
	cout << '\n';

	a = c + 1234;		// as c.operator+ (1234)
	
	a.display();
	cout << " = ";
	b.display();
	cout << " + ";
	cout << "1234\n";
	
}

					 

	