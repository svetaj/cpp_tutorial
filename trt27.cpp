// References as a Pointers
#include <iostream.h>

main()
{
        int actualint = 123;
        int *const intptr = &actualint;         // Constant pointer
                                                // points to actualint
//      int  &otherint = actualint;

	cout << '\n' << actualint;
        cout << '\n' << *intptr;
	actualint++;
	cout << '\n' << actualint;
        cout << '\n' << *intptr; 
}
