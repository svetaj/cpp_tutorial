// Usage of const when declaring function to prevent modifying parameters

int readonly (const struct Node *nodeptr)
{
        struct Node *writeptr;          // Ordinary pointer

        writeptr = nodeptr;             // Error - illegal assignment
}
