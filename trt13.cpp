// Declaring a variable near its first reference
#include <iostream.h>

void main()
{
        cout << "Enter a number: ";
        int n;
        cin >> n;
        cout << "the number is: " << n;

        for( int ctr = 0; ctr < 5; ctr++)
                cout << "\nctr = " << ctr;

//      if( int i == 0 )        // Error
//              ;
//      while( int j == 0 )     // Error
//              ;

}


