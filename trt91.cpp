#include <iostream.h>
#include <string.h>

struct Record
{
	char name[30];
	char number[10];
};

const int MAXLENGTH = 100;

class PhoneList
{
friend class PhoneIter;
public:
	PhoneList();
	int add (const Record &newRec);
	Record *Search (char *searchKey);
private:
	Record aray[MAXLENGTH];
	int firstEmpty;			// First unused element
};

PhoneList::PhoneList()
{
	firstEmpty = 0;
}

int PhoneList::add (const Record &newRec)
{
	if (firstEmpty < MAXLENGTH -1)
	{
		aray [firstEmpty++] = newRec;
		return 1;						// OK
	}
	else
		return 0;
}

Record *PhoneList::Search (char *searchKey)
{
	for (int i = 0; i < firstEmpty; i++)
		if (!strcmp (aray[i].name, searchKey))
			return &aray[i];
			
	return 0;
}


class PhoneIter
{
public:
	PhoneIter (PhoneList &m);
	Record *getFirst();
	Record *getLast();
	Record *getNext();
	Record *getPrev();
private:
	PhoneList *const mine;		// Pointer to a PhoneList object
	int currIndex;
};

PhoneIter::PhoneIter (PhoneList &m)
	: mine ( &m )				// Initialize the constant member
{
	currIndex = 0; 
}
	
Record *PhoneIter::getFirst()
{
	currIndex = 0;
	return &(mine->aray[currIndex]);
}

Record *PhoneIter::getLast()
{
	currIndex = mine->firstEmpty -1;
	return &(mine->aray[currIndex]);
}	

Record *PhoneIter::getNext()
{
	if (currIndex < mine->firstEmpty - 1)		
    {
    	currIndex++;
    	return &(mine->aray[currIndex]);
    }
    else
    	return 0;
}


Record *PhoneIter::getPrev()
{
	if (currIndex > 0)
	{
		currIndex--;
		return &(mine->aray[currIndex]);
	}
	else
		return 0;
}

		
void printList (PhoneList &aList)
{
	Record *each;
	PhoneIter anIter (aList);
	
	each = anIter.getFirst();  
	cout << "======== Phone List =========\n";
	cout << each->name << ' ' << each->number << '\n';
	while ( each = anIter.getNext() )
	{
		cout << each->name << ' ' << each->number << '\n';
	}
	cout << "=============================\n";
}


main()
{       
	PhoneList myDiary;       
	Record temp;  
	char nm[30];
	
	for (int i=0; i<5; i++) {
		cout << i << ". Enter name\n";
		cin >> temp.name;
		cout << i << ". Enter number\n";
		cin >> temp.number; 
		myDiary.add(temp);   
	}
	printList(myDiary);   
	cout << "Enter name to be found\n";
	cin >> nm;
	temp = *myDiary.Search (nm);
	cout << "FOUND: " << temp.name << " " << temp.number;
	return 0;
}


		





