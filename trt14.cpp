// Variable declaration placement and scope of variable
#include <iostream.h>

void main()
{
     for( int lineno = 0; lineno < 3; lineno++ )
     {
          int temp = 22;
          cout << "\nThis is line number " << lineno
               << " and temp is " << temp;
     }
     if( lineno == 4 )          // lineno still accessible
          cout << "\nOops";
     // Cannot access temp
     cout << "\nThis is line number " << lineno;     // OK
//   cout << " and temp is " << temp;    // Compiler error, temp not defined
}

