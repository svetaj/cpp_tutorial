#include <iostream.h>

class A
{
friend class C;
public:
     A() { ia = 0; }
     A( int ix ) { ia = ix; }
     void showA() { cout << "A: " << ia << '\n'; }
     ~A() {}
private:
     int ia;
};

class B
{
public:
      B() { ib = 0; }   
      B(int ix, int iy);
      void showB();
      ~B() {}
private:
      int ib;
      A xa;
};

B::B( int x , int y )
    : xa( y )
{
     ib = x;
}

void B::showB()
{
     cout << "------------------\n";
     cout << "B: " << ib << "\n";
     xa.showA();
     cout << "------------------\n";
}

class C
{
public:
     C() { ic = 0; }
     C( int ix ) { ic = ix; }
     void showC() { cout << "C: " << ic << '\n'; }
     void showA(A &x) { cout << "A: " << x.ia << '\n'; }
     ~C() {}
private:
     int ic;
};

class D : public A
{
public:
     D() { id = 0; }
     D( int ix , int iy );
     void showD();
     ~D() {}
private:
     int id;
};

D::D( int x , int y )
    : A( y )
{
     id = x;
}

void D::showD()
{
     cout << "------------------\n";
     cout << "D: " << id << "\n";
     showA();
     cout << "------------------\n";
}


void main()
{
     A x( 999 );
     B y( 888, 777 );
     C z( 444 );
     D p( 123, 456 );
     x.showA();
     y.showB();
     z.showC();
     z.showA(x);
     p.showD();
}
