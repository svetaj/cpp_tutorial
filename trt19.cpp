// enum as a data type
#include <iostream.h>

enum color { red, orange, yellow, green, blue, violet };

void main()
{
     color myFavorite;

     myFavorite = blue;
     cout << myFavorite;
}

