// References as Return Values
#include <iostream.h>

int mynum = 0;	// Global variable

int &num()
{
	return mynum;
}

void main()
{                                                       
	int i;
	
	i = num();
	cout << "mynum = " << mynum << '\n';
	num() = 5;	// mynum set to 5
	cout << "mynum = " << mynum << '\n';
}

    
