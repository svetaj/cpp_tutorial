#include <iostream.h>
#include <fstream.h>
#include <stdlib.h>

void greska( char *p, char *p2)
{
        cerr << p << " " << p2 << '\n';
        exit(1);
}

void main( int argc, char *argv[])
{
        if (argc != 3)
                greska( "pogresan broj argumenata", " ");

        filebuf d1;
        if ( d1.open( argv[1], ios::in ) == 0 )
                greska("ne moze da se otvori ulazna datoteka", argv[1]);
        istream iz(&d1);

        filebuf d2;
        if ( d2.open( argv[2], ios::out ) == 0 )
                greska("ne moze da se formira izlazna datoteka", argv[2]);
        ostream ka(&d2);

        char ch;
        while (iz.get(ch))
                ka.put(ch);

        if ( !iz.eof() || ka.bad() )
                greska(" greska", " ");
}
