/* Example of implementing related types in C++ */
#include <iostream.h>
#include <string.h>

class Employee
{
public:
	Employee();
	Employee (const char *nm);
        char *getName() {return name;}
private:
	char name[30];
};
 
Employee::Employee (const char *nm)
{
	strcpy(name, nm);
}

class WageEmployee : public Employee
{
public:
	WageEmployee ();
	WageEmployee (const char *nm);
	void setWage (float wg) {wage = wg;}
	void setHours (float hrs) {hours = hrs;} 
	void WageEmployee::printName() const; 
	float computePay() const { return wage * hours;}
private:
	float wage;
	float hours;
};

WageEmployee::WageEmployee (const char *nm)
	:Employee (nm)
{
	wage = 0.0;
	hours = 0.0;
}	

WageEmployee::WageEmployee ()
	:Employee (" ")
{
	wage = 0.0;
	hours = 0.0;
}	


void WageEmployee::printName() const
{
	cout << "Worker's name: " << getName() << '\n'; // Call Employee::getName 
}
 
class SalesPerson : public WageEmployee
{
public:
	SalesPerson (const char *nm);
	void setCommision (float comm) {commision = comm;}
	void setSales (float sales) {salesMade = sales;}     
	float SalesPerson::computePay() const 
	{ return WageEmployee::computePay() + commision * salesMade;}
private:
	float commision;
	float salesMade;
};

SalesPerson::SalesPerson (const char *nm)
	:WageEmployee (nm)
{               
	commision = 0.0;
	salesMade = 0.0;
}	

class Manager : public Employee
{
public:
	Manager (const char *nm);
	void setSalary (float salary) {weeklySalary = salary;}
	float Manager::computePay() const { return weeklySalary;}
private:
	float weeklySalary;
};

Manager::Manager (const char *nm)
	: Employee (nm)
{                   
	weeklySalary = 0.0;
}



main()
{
	WageEmployee aWorker ("Bill Shapiro");
	char *str;
	
	aWorker.setHours (40.0);		// Call WageEmployee::setHours
	str = aWorker.getName();		// Call Employee::getName 
	cout << "Worker name = " << str << '\n';   
	aWorker.printName();
	
	SalesPerson aSeller ("John Smith");
	
	aSeller.setHours (40.0);
	aSeller.setWage (6.0);
	aSeller.setCommision (0.05);
	aSeller.setSales (2000.0);
	
	// Call SalesPerson::computePay
	cout << "Seller salary: " << (int) aSeller.computePay() << '\n';	
	cout << "Seller base salary: " << (int) aSeller.WageEmployee::computePay() << '\n';
	          
	WageEmployee xWorker;
	SalesPerson xSeller ("John Smith");
	
	xWorker = xSeller;	// Convert SalesPerson to WageEmployee 
						// 		derived => base
	
	Employee *empPtr;
	WageEmployee yWorker ("Bill Shapiro");
	SalesPerson ySeller ("John Smith");
	Manager yBoss ("Mary Brown");
	
	empPtr = &yWorker;			// Convert WageEmployee * to Employee *
	empPtr = &ySeller;			// Convert SalesPerson * to Employee *
	empPtr = &yBoss;			// Convert Manager * to Employee *
	
	SalesPerson zSeller ("John Smith");
	SalesPerson *salePtr;
	WageEmployee *wagePtr;
	
	salePtr = &zSeller;
	wagePtr = &zSeller;
	
	wagePtr->setHours (40.0);		// Call WageEmployee::setHours
	salePtr->setWage (6.0);			// Call WageEmployee::setWage
//	wagePtr->setSales (1000.0);		// Error: no WageEmployee::setSales
	
	salePtr->setSales (1000.0);		// Call SalesPerson::setSales
	salePtr->setCommision (0.05);	// Call SalesPerson::setCommision
	
	float base, total;
	
	base = wagePtr->computePay();	// Call WageEmployee::computePay	
	total = salePtr->computePay();	// Call SalesPerson::computePay	
	
	
	WageEmployee *wagePtr1 = &aSeller;
	SalesPerson *salePtr1;
	
	salePtr1 = (SalesPerson *) wagePtr;		// Explicit cast required
											// 		base => derived
											
        Employee *empPtr1 = &aWorker;
	SalesPerson *salesPtr;      
	
//      salePtr = (SalesPerson *) empPtr1;               // Legal, but incorrect
//	salePtr->setCommission (0.05);			// Error: aWorker has no setCommision member
	
	return 0;	                  
	
}
 
 
