class Base
{
public:
protected:
	int secret;
private:
	int topSecret;
};

class Derived : public Base
{
public:
	void func();
};

void Derived::func()
{
	secret = 1;			// Can access protected member
	topSecret = 1;		// Error: can't access private member
}
 
void main()
{
	Base aBase;
	Derived aDerived;
	
	aBase.secret = 1;		// Error: can't access protected member
	aBase.topSecret = 1;	// Error: can't access private member
	aDerived.secret = 1;	// Error: can't access protected member 
							// in derived class either
}

