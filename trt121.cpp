/* Example of implementing related types in C++ */
#include <iostream.h>
#include <string.h>

const int MAXLENGTH = 100;

class Employee
{
public:
	Employee();
	Employee (const char *nm);
        char *getName() {return name;}   
	virtual float computePay() const;
	virtual ~Employee() {};
private:
	char name[30];
};
 
Employee::Employee (const char *nm)
{
	strcpy(name, nm);
}

float Employee::computePay() const
{
	cout << "No salary computation defined\n";
	return 0.0;
}

class WageEmployee : public Employee
{
public:
	WageEmployee ();
	WageEmployee (const char *nm);
	void setWage (float wg) {wage = wg;}
	void setHours (float hrs) {hours = hrs;} 
	void WageEmployee::printName() const; 
	float computePay() const; 				// Implicitly virtual
private:
	float wage;
	float hours;
};

WageEmployee::WageEmployee (const char *nm)
	:Employee (nm)
{
	wage = 10.0;
	hours = 15.0;
}	

WageEmployee::WageEmployee ()
	:Employee (" ")
{
	wage = 0.0;
	hours = 0.0;
}	


void WageEmployee::printName() const
{
	cout << "Worker's name: " << getName() << '\n'; // Call Employee::getName 
}

float WageEmployee::computePay() const 
{ 
	return wage * hours;
} 

class SalesPerson : public WageEmployee
{
public:
	SalesPerson (const char *nm);
	void setCommision (float comm) {commision = comm;}
	void setSales (float sales) {salesMade = sales;}     
	float SalesPerson::computePay() const; 		// Implicitly virtual 
private:
	float commision;
	float salesMade;
};

SalesPerson::SalesPerson (const char *nm)
	:WageEmployee (nm)
{               
	commision = 50.0;
	salesMade = 50.0;
}	

float SalesPerson::computePay() const
{ 
	return WageEmployee::computePay() + commision * salesMade;
}

class Manager : public Employee
{
public:
	Manager (const char *nm);
	void setSalary (float salary) {weeklySalary = salary;}
	float Manager::computePay() const;		// Implicitly virtual
private:
	float weeklySalary;
};

Manager::Manager (const char *nm)
	: Employee (nm)
{                   
	weeklySalary = 300.0;
}

float Manager::computePay() const 
{ 
	return weeklySalary;
}

class EmployeeList
{
friend class EmployeeIter;
public:
	EmployeeList();
	int add (Employee *newEmp);
private:
	Employee *aray[MAXLENGTH];
	int firstEmpty;			// First unused element
};


EmployeeList::EmployeeList()
{
	firstEmpty = 0;
}

int EmployeeList::add(Employee *newEmp)
{
	if (firstEmpty+1 < MAXLENGTH)
	{
		aray[firstEmpty] = newEmp;
		firstEmpty++;
		return 0;
	}
	else
		return 1; 
}

class EmployeeIter
{
public:
	EmployeeIter (EmployeeList &m);
	Employee *getFirst();
	Employee *getLast();
	Employee *getNext();
	Employee *getPrev();
private:
	EmployeeList *const mine;		// Pointer to a EmployeeList object
	int currIndex;
};

EmployeeIter::EmployeeIter (EmployeeList &m)
	: mine ( &m )				// Initialize the constant member
{
	currIndex = 0; 
}
	
Employee *EmployeeIter::getFirst()
{
	currIndex = 0;
	return (mine->aray[currIndex]);
}

Employee *EmployeeIter::getLast()
{
	currIndex = mine->firstEmpty -1;
	return (mine->aray[currIndex]);
}	

Employee *EmployeeIter::getNext()
{
	if (currIndex < mine->firstEmpty - 1)		
    {
    	currIndex++;
    	return (mine->aray[currIndex]);
    }
    else
    	return 0;
}


Employee *EmployeeIter::getPrev()
{
	if (currIndex > 0)
	{
		currIndex--;
		return (mine->aray[currIndex]);
	}
	else
		return 0;
}

void printNames (EmployeeList &dept)
{
	int count = 0;
	Employee *person;
	EmployeeIter anIter (dept);		// Iterator object
	 
	person = anIter.getFirst();
	cout << count << ' ' << person->getName() << '\n';
	 
	while ( person = anIter.getNext() )	
	{
		count++;
		cout << count << ' '
			 << person->getName() << '\n';
	}
}

float computePayroll (EmployeeList &dept)
{
	float payroll = 0;
	Employee *person;
	EmployeeIter anIter (dept);
	
	person = anIter.getFirst();
	payroll += person->computePay();
	while (person = anIter.getNext())
	{
		// Call appropriate function
		// for each type of employee
		payroll += person->computePay();
	}
	
	return payroll;
}
	
main()
{

	EmployeeList myDept;
	WageEmployee *wagePtr;
	SalesPerson *salePtr;
	Manager *mgrPtr;
	
	// Allocate new objects
	wagePtr = new WageEmployee ("Bill Shapiro");
	salePtr = new SalesPerson ("John Smith");
	mgrPtr = new Manager ("Mary Brown");	
	// Add them to the list
	myDept.add (wagePtr);
	myDept.add (salePtr);
	myDept.add (mgrPtr);
	
	// Allocate new objects
	wagePtr = new WageEmployee ("Trta Mrta");
	salePtr = new SalesPerson ("Prda Smrda");
	mgrPtr = new Manager ("Keba Jeba");
	// Add them to the list
	myDept.add (wagePtr);
	myDept.add (salePtr);
	myDept.add (mgrPtr);

	printNames (myDept);

	Employee *empPtr;
	float salary;
	
	empPtr = wagePtr;
	salary = empPtr->computePay();		// Call WageEmployee::computePay
	empPtr = salePtr;
	salary = empPtr->computePay();		// Call SalesPerson::computePay
	empPtr = mgrPtr;
	salary = empPtr->computePay();		// Call Manager::computePay

	cout << "Payroll = " << (int) computePayroll (myDept) << '\n';	
			
	return 0;	                  
	
}
 
 
