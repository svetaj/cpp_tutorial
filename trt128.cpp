#include <iostream.h>
#include <string.h>
#include <math.h>

#define MAXLIST 100

class SortableObject
{
public:  
      virtual int isEqual( const SortableObject &other ) const = 0;
      virtual int isLessThan( const SortableObject &other ) const = 0;
      virtual void printObject() const = 0;
      virtual void *getObject() const = 0;
};

class SortableName : public SortableObject
{
public:  
      SortableName() { strncpy(name, "sveta", 5); }
      SortableName( char *nm ) { strncpy(name, nm, 30); }
      void printObject() const { cout << "name = " << name <<'\n'; }
      int isEqual( const SortableObject &other ) const;
      int isLessThan( const SortableObject &other ) const;
      void *getObject() const { return (void *)name; }
private:
      char name[30];
};

int SortableName::isEqual (const SortableObject &other) const
{
      return (strncmp (name, (const char *)other.getObject(), 30) == 0);
};

int SortableName::isLessThan (const SortableObject &other) const
{
      return (strncmp (name, (const char *)other.getObject(), 30) < 0);
};
   
class SortedList
{
public:
      SortedList();
      void addItem (SortableObject &newItem);
      void printSorted();
private:
      int firstEmpty;
      SortableObject *list[MAXLIST];
};

SortedList::SortedList()
{
      firstEmpty = 0;
}                               

void SortedList::addItem (SortableObject &newItem)
{
      if (firstEmpty == 0)
      {
           list[0] = &newItem;
           firstEmpty++;
      }
      else                      
      {
           int firstOld;    
           firstOld = firstEmpty;   
           for (int i=0; i < firstEmpty; i++) 
           {
                 if (newItem.isLessThan(*list[i])) {
                         for (int j=firstEmpty; j>i; j--)
                              list[j] = list[j-1];
                         list[i] = &newItem;
                         firstEmpty++;
                         break;
                 }
           }
           if ( firstOld == firstEmpty) 
           {
                 list[firstEmpty] = &newItem;
                 firstEmpty++;
           } 
      }
}			  

void SortedList::printSorted()
{
     for (int i = 0; i < firstEmpty; i++) 
          list[i]->printObject();      
}

class Cmplx
{
public:
      Cmplx() { x = 0.; y = 0.; }
      Cmplx( double xx, double yy ) { x = xx; y = yy; }
      show() { cout << "(" << x << ", " << y << ")\n"; }
      double AbsVal () { return sqrt( x*x + y*y); }
private:
      double x;
      double y;
};

class SortableCmplx : public SortableObject
{
public:  
      SortableCmplx();
      SortableCmplx( double xx, double yy );
      void printObject() const { z.show(); }
      int isEqual( const SortableObject &other ) const;
      int isLessThan( const SortableObject &other ) const;
      void *getObject() const { return (void *)&z; }
private:
      Cmplx z;
};

SortableCmplx::SortableCmplx( double xx, double yy )
      : z (xx, yy)
{ }

SortableCmplx::SortableCmplx()
      : z (0., 0.)
{ }


int SortableCmplx::isEqual (const SortableObject &other) const
{
      return (z.AbsVal() == ((Cmplx *)other.getObject())->AbsVal());
};

int SortableCmplx::isLessThan (const SortableObject &other) const
{
      return (z.AbsVal() < ((Cmplx *)other.getObject())->AbsVal());
};


main()
{ 
      int i;

      cout << "############# Ne sortirana lista imena #############\n"; 
      SortableName imena[10] = {"sisoje", "pera",  "mika",  "alempije",
                                "kukoje", "mlakoje", "prtoje", "avram",
                                "slobo", "jeboslava"};
      for (i=0; i<10; i++)
           imena[i].printObject();
      cout << "############# Ne sortirana lista imena #############\n"; 
	
      cout << "@@@@@@@@@@@@@@@ Sortirana lista imena @@@@@@@@@@@@@@\n"; 
      SortedList xList;   
      for (i=0; i<10; i++)
           xList.addItem (imena[i]);
      xList.printSorted();    
      cout << "@@@@@@@@@@@@@@@ Sortirana lista imena @@@@@@@@@@@@@@\n"; 
	

      cout << "############# Ne sortirana lista kompleksnih brojeva #############\n"; 
      SortableCmplx brojevi[10] = { SortableCmplx( 3., -4.),
                                    SortableCmplx( 100., 55.),
                                    SortableCmplx( -3., -4.),
                                    SortableCmplx( 100., 56.),
                                    SortableCmplx( 22., 88.),
                                    SortableCmplx( 100., 57.),
                                    SortableCmplx( 11., -4.),
                                    SortableCmplx( 100., 52.),
                                    SortableCmplx( 3., -99.),
                                    SortableCmplx( 100., 51.),
                                  };
      for (i=0; i<10; i++)
           brojevi[i].printObject();
      cout << "############# Ne sortirana lista kompleksnih brojeva #############\n"; 
	
      cout << "@@@@@@@@@@@@@@@ Sortirana lista kompleksnih brojeva @@@@@@@@@@@@@@\n"; 
      SortedList yList;   
      for (i=0; i<10; i++)
           yList.addItem (brojevi[i]);
      yList.printSorted();    
      cout << "@@@@@@@@@@@@@@@ Sortirana lista kompleksnih brojeva @@@@@@@@@@@@@@\n"; 

      return 0;
}

 
