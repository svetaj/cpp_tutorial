// BAD TECHIQUE: Member function that returns a reference

#include <iostream.h>
#include <string.h>

class Date
{
public: 
        Date ();                        
        Date (int mn, int dy, int yr);  
        int &month();                   // Set/get month                                        
        void display();                 // Print date
        ~Date();                        // Destructor
private:
        int month_member,               // Private data members
            day_member,
            year_member;           
};

// some useful functions
inline int max(int a, int b)
{
	if (a > b) return a;
	return b;
}

inline int min(int a, int b)
{
	if (a < b) return a;
	return b;
}


int &Date::month()
{
        month_member = max( 1, month_member );
        month_member = min( month_member, 12 );
        return month_member;
}


// -------- The constructor
Date::Date( int mn, int dy, int yr )
{
	static int length[] = {0, 31, 28, 31, 30, 31, 30,
                                  31, 31, 30, 31, 30, 31};
	// Ignore leap years for simplicity
        month_member = max( 1, mn );
        month_member = min( month_member, 12 );
	
        day_member = max( 1, dy );
        day_member = min( day_member, length[month_member] );
	
        year_member = max( 1, yr );
}		

Date::Date()
{
        month_member = day_member = year_member = 1;
}
		
// -------- The destructor 
Date::~Date()
{
	// do nothing
}


// --------- Member function to print date
void Date::display()
{
	static char *name[] = 
	{
		"zero", "January", "Febryary", "March", "April", "May",
		"June", "July", "August", "September", "October",
		"November", "December"
	};
		
        cout << name[month_member] << ' '
             << day_member << ", "
             << year_member << '\n';
}


// BAD TECHNIQUE: using member function that returns a reference
void main()
{
        int i;
        Date deadline( 3, 10, 1980 );
        deadline.display();
        
        i = deadline.month();           // Read month value
        cout << "Month =" << i << '\n';
        deadline.month() = 4;           // Modify month value
        deadline.display();
        deadline.month()++;             // Increment
        deadline.display();
}
