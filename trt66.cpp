// The free store and built-in types

#include <iostream.h>
#include <string.h>

main()
{
        int *ip;

        ip = new int;                    // Allocate an integer
        *ip = 999;                       // Use ip
        cout << "\nip = " << *ip;
        delete ip;

// ---------------------------------------------------------------

        int length;
        char *cp;

        // Assign value to length, depending on user input
        cout << "\nEnter length of cp";
        cin >> length;
        cp = new char[length];          // Allocate an array of chars
        strcpy( cp, "abcdefghijklmnopqrstuvwxyz");         // Use cp
        cout << "\ncp = " << cp;
        delete [] cp;

// ---------------------------------------------------------------

        int (*matrix)[10];
        int size;

        //Assign value to size, depending on user input
        cout << "\nEnter size of matrix";
        cin >> size;
        matrix = new int[size][10];     // Allocate a 2-D array
        for (int i = 0; i<size; i++)    
            for (int j = 0; j<10; j++)  
            {
                  matrix[i][j] = i*j;                  // Use matrix
                  cout << "\nmatrix[" << i << "]["
                                      << j << "] = "
                                      << matrix[i][j];
            }
        delete [] matrix;
}
