#include "string.h"

void consume( const String &parm )
{                                       // const String &parm = myString
        parm.display();
}

void main()
{
        String myString( "here's my string" );

        consume( myString );
}
