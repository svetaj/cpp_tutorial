#include <stdio.h>

struct date
{
        int month;
        int day;
        int year;
};

void display_date( struct date *dt );

main()
{
        struct date my_date;

        my_date.month = 1;
        my_date.day = 23;
        my_date.year = 1985;

        display_date( &my_date );
}

void display_date( struct date *dt )
{
        static char *name[] =
        {
                "zero", "January", "February", "March", "April", "May",
                "June", "July", "August", "September", "October",
                "November", "December"
        };

        printf( "%s %d, %d", name[dt->month], dt->day, dt->year );
}


