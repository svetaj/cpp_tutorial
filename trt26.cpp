// The reference

#include <iostream.h>


void main()
{
	int actualint = 123;
	int &otherint = actualint;  
	
	cout << '\n' << actualint;
	cout << '\n' << otherint;
	otherint++;
	cout << '\n' << actualint;
	cout << '\n' << otherint;
	actualint++;
	cout << '\n' << actualint;
	cout << '\n' << otherint;	
}


