// Overloading the + operator
#include <stdlib.h>
#include <math.h>
#include <iostream.h>

class Fraction
{
public:
	Fraction();
	Fraction (long num, long den = 1);
	void display() const;
	friend Fraction operator+ (const Fraction &first, 
							   const Fraction &second);
private:
	long numerator, denominator;
};

// ------------- Default constructor
Fraction::Fraction()
{
	numerator = 0;
	denominator = 1;
}

// ------------- Constructor
Fraction::Fraction (long num, long den)
{
	if (den == 0)
		den = 1;
	numerator = num;
	denominator = den;
	if (den < 0)
	{
		numerator = -numerator;
		denominator = -denominator;
	}
}

// ------------- Function to print a Fraction
void Fraction::display() const
{
	cout << numerator << '/' << denominator; 
}


// ------------- Overloaded + operator
Fraction operator+ (const Fraction &first, const Fraction &second)
{
	long mult1, mult2;
	
	mult1 = first.denominator;
	mult2 = second.denominator;
	
	return Fraction (first.numerator * mult2 + second.numerator * mult1, 
					 first.denominator * second.denominator);
}


void main()
{
	Fraction a1(2);	// Equivalent to Fraction a1 (2, 1)
	cout << " a1 = ";
	a1.display();
	cout << '\n';	
	
	a1 = 7;			// Equivalent to a1 = Fraction (7)
					// 				 a1 = Fraction (7, 1)
	cout << " a1 = ";
	a1.display();
	cout << '\n';	
		
	Fraction a, b (2, 3), c (4, 5);
	
	a = b + c;			// Okay as is
	cout << " a = ";
	a.display();
	cout << '\n';	

	a = b + 1234;		// a = b + Fraction (1234);
	cout << " a = ";
	a.display();
	cout << '\n';	

	a = 1234 + b;		// a = Fraction (1234) + b;
	cout << " a = ";
	a.display();
	cout << '\n';	

	a = 1234 + 5678;	// a = Fraction (6912);
	cout << " a = ";
	a.display();
	cout << '\n';	

}                      


					 

	