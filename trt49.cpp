// The creation and destruction of objects

#include <iostream.h>
#include <string.h>

// --------- a Date class

class Demo
{
public:
        Demo ( const char *nm );        // Constructor
        ~Demo();                        // Destructor
private:
	char name[20];
};

Demo::Demo( const char *nm)
{
	strncpy (name, nm, 20);
	cout << "Constructor called for " << name << '\n';
}

Demo::~Demo()
{
	cout << "Destructor called for " << name << '\n';
}

void func()
{
	Demo localFuncObject ("localFuncObject");
	static Demo staticObject ("staticObject");
	
	cout << "Inside func\n";
}

Demo globalObject ( "globalObject" );

void main()
{
	Demo localMainObject ("localMainObject");
	
	cout << "In main, before calling func\n";
	func();
	cout << "In main, after calling func\n";
}



    
