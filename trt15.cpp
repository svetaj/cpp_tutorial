// Scope resolution operator
#include <iostream.h>

int amount = 123;       // A global variable

void main()
{
     int amount = 456;  // A local variable

     cout << "Global " << ::amount;  // Print the global variable
     cout << '\n';
     cout << "Local  " << amount;    // Print the local variable
}

