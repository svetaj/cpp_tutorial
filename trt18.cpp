// The const qualifier, more of
#include <iostream.h>

void main()
{
     char mybuf[] = "xyz";
     char yourbuf[] = "pqr";
     char *const ptr1 = mybuf;   // const pointer
     *ptr1 = 'a';                // Change char that p points to; legal
//   ptr1 = yourbuf;             // Change pointer; error
     cout << ptr1;

     cout << '\n';

     const char *ptr2 = mybuf;   // pointer to const
     ptr2 = yourbuf;             // Change pointer; legal
//   *ptr2 = 'a';                // Change char that p points to; error
     cout << ptr2;
}

