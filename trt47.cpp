// The date class
#include <iostream.h>

// --------- a Date class

class Date
{
public:
	Date (int mn, int dy, int yr );	// Constructor
        void display();                 // Function to print date
        ~Date();                        // Destructor
private:
        int month, day, year;           // Private data member 
        int daysSoFar();                // Private member function
};

// some useful functions
inline int max(int a, int b)
{
	if (a > b) return a;
	return b;
}

inline int min(int a, int b)
{
	if (a < b) return a;
	return b;
}

// -------- The constructor
Date::Date (int mn, int dy, int yr)
{
        static int length[] = { 0, 31, 28, 31, 30, 31, 30,
                               31, 31, 30, 31, 30, 31 };
	// Ignore leap years for simplicity
	month = max (1, mn);
	month = min (month, 12);
	
	day = max (1, dy);
	day = min (day, length[month]);
	
	year = max (1, yr);
}

// --------- Member function to print date
void Date::display()
{
        cout << daysSoFar()       // Call private member function
             << " " << year;  
}

// ------------ Compute numbers of days elapsed
int Date::daysSoFar()
{
	int total = 0;
        static int length[] = { 0, 31, 28, 31, 30, 31, 30,
                               31, 31, 30, 31, 30, 31 };
	for (int i = 1; i < month; i++)
		total += length[i];
		
	total += day;
	return total;
}	
		
// -------- The destructor 
Date::~Date()
{
	// do nothing
}

// =========== Program that demonstrates the Date class
void main()
{
        Date myDate (3, 12, 1985);      // Declare a Date
	Date yourDate (23, 259, 1966);	// Declare a invalid Date 
	
	myDate.display();
	cout << '\n';
	yourDate.display();
	cout << '\n';           
}						   	  



    
