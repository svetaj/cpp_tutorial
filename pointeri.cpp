#include <iostream.h>

main()
{
        int *a;
        a = new int[7];

        int (*b)[4];
        b = new int[7][4];

        int *c[4];
        c[0] = new int[3];
        c[1] = new int[7];
        c[2] = new int[2];
        c[3] = new int[4];

        int **d;
        (*d) = new (int *)[4];
        d[0] = new int[3];
        d[1] = new int[7];
        d[2] = new int[2];
        d[3] = new int[4];
}
