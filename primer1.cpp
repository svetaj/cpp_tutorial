#include <iostream.h>

class A
{
public:
	int a1;
protected:
	int a2;
private:
	int a3;
};

class B1 : public A
{
public:
	void funB11(int x) { a1 = x; }
	void funB12(int x) { a2 = x; }
//	void funB13(int x) { a3 = x; }		// ERROR (1)		
};

class B2 : protected A
{
public:
	void funB21(int x) { a1 = x; }
	void funB22(int x) { a2 = x; }
//	void funB23(int x) { a3 = x; }		// ERROR (1)
};

class B3 : private A
{
public:
	void funB31(int x) { a1 = x; }
	void funB32(int x) { a2 = x; }
//	void funB33(int x) { a3 = x; }		// ERROR (1)
};

class C11 : public B1
{
public:
	void funC111(int x) { a1 = x; }
	void funC112(int x) { a2 = x; }
//	void funC113(int x) { a3 = x; }		// ERROR (1)
	void fffC111(int x) { funB11(x); }
	void fffC112(int x) { funB12(x); }
//	void fffC113(int x) { funB13(x); }	// ERROR (4)	
};

class C12 : public B2
{
public:
	void funC121(int x) { a1 = x; }
	void funC122(int x) { a2 = x; }
//	void funC123(int x) { a3 = x; }		// ERROR (1)		
	void fffC121(int x) { funB21(x); }
	void fffC122(int x) { funB22(x); }
//	void fffC123(int x) { funB23(x); }	// ERROR (5)  	
};

class C13 : public B3
{
public:
//	void funC131(int x) { a1 = x; }		// ERROR (2)		
//	void funC132(int x) { a2 = x; }		// ERROR (3)     
//	void funC133(int x) { a3 = x; }		// ERROR (1)		
	void fffC131(int x) { funB31(x); }
	void fffC132(int x) { funB32(x); }
//	void fffC133(int x) { funB33(x); }	// ERROR (6)  	
};


class C21 : protected B1
{
public:
	void funC211(int x) { a1 = x; }
	void funC212(int x) { a2 = x; }
//	void funC213(int x) { a3 = x; }		// ERROR (1)		
	void fffC211(int x) { funB11(x); }
	void fffC212(int x) { funB12(x); }
//	void fffC213(int x) { funB13(x); }	// ERROR (4)  	
};

class C22 : protected B2
{
public:
	void funC221(int x) { a1 = x; }
	void funC222(int x) { a2 = x; }
//	void funC223(int x) { a3 = x; }		// ERROR (1)		
	void fffC221(int x) { funB21(x); }
	void fffC222(int x) { funB22(x); }
//	void fffC223(int x) { funB23(x); }	// ERROR (5)  	
};

class C23 : protected B3
{
public:
//	void funC231(int x) { a1 = x; }		// ERROR (2)		
//	void funC232(int x) { a2 = x; }		// ERROR (3)     
//	void funC233(int x) { a3 = x; }		// ERROR (1)		
	void fffC231(int x) { funB31(x); }
	void fffC232(int x) { funB32(x); }
//	void fffC233(int x) { funB33(x); }	// ERROR (6)  	
};

class C31 : private B1
{
public:
	void funC311(int x) { a1 = x; }
	void funC312(int x) { a2 = x; }
//	void funC313(int x) { a3 = x; }		// ERROR (1)		
	void fffC311(int x) { funB11(x); }
	void fffC312(int x) { funB12(x); }
//	void fffC313(int x) { funB13(x); }	// ERROR (4)  	
};

class C32 : private B2
{
public:
	void funC321(int x) { a1 = x; }
	void funC322(int x) { a2 = x; }
//	void funC323(int x) { a3 = x; }		// ERROR (1)		
	void fffC321(int x) { funB21(x); }
	void fffC322(int x) { funB22(x); }
//	void fffC323(int x) { funB23(x); }	// ERROR (5)  	
};

class C33 : private B3
{
public:
//	void funC331(int x) { a1 = x; }		// ERROR (2)		
//	void funC332(int x) { a2 = x; }		// ERROR (3)     
//	void funC333(int x) { a3 = x; }		// ERROR (1)		
	void fffC331(int x) { funB31(x); }
    void fffC332(int x) { funB32(x); }
//	void fffC333(int x) { funB33(x); }	// ERROR (6)  	
};

void main()
{
}


// ERROR (1)  'a3' : cannot access private member declared in class 'A'
// ERROR (2)  'a1' not accessible because 'B3' uses 'private' to inherit from 'A'
// ERROR (3)  'a2' not accessible because 'B3' uses 'private' to inherit from 'A'
// ERROR (4)  'funB13' : undeclared identifier
// ERROR (5)  'funB23' : undeclared identifier
// ERROR (6)  'funB33' : undeclared identifier


