// Overloading the + operator
#include <stdlib.h>
#include <math.h>
#include <iostream.h>

class Fraction
{
public:
	Fraction();
	Fraction (long num, long den);
	void display() const;       
	float cvToFloat() const;
	friend Fraction operator+ (const Fraction &first, 
							   const Fraction &second);
	friend Fraction operator+ (long first, 
							   const Fraction &second);
	friend Fraction operator+ (const Fraction &first, 
							   long second);
private:
	long numerator, denominator;
};

// ------------- Default constructor
Fraction::Fraction()
{
	numerator = 0;
	denominator = 1;
}

// ------------- Constructor
Fraction::Fraction (long num, long den)
{
	if (den == 0)
		den = 1;
	numerator = num;
	denominator = den;
	if (den < 0)
	{
		numerator = -numerator;
		denominator = -denominator;
	}
}

// ------------- Function to print a Fraction
void Fraction::display() const
{
	cout << numerator << '/' << denominator; 
}

float Fraction::cvToFloat() const
{
	return (float) numerator / (float) denominator;
}

// ------------- Overloaded + operator
Fraction operator+ (const Fraction &first, const Fraction &second)
{
	long mult1, mult2;
	
	mult1 = first.denominator;
	mult2 = second.denominator;
	
	return Fraction (first.numerator * mult2 + second.numerator * mult1, 
					 first.denominator * second.denominator);
}

// ------------- Overloaded + operator
Fraction operator+ (long first, const Fraction &second)
{
	long mult1, mult2;
	
	mult1 = first;
	mult2 = second.denominator;
	
	return Fraction (first * mult2 + second.numerator * mult1, 
					 second.denominator);
}

// ------------- Overloaded + operator
Fraction operator+ (const Fraction &first, long second)
{
	long mult1, mult2;
	
	mult1 = first.denominator;
	mult2 = second;
	
	return Fraction (first.numerator * mult2 + second * mult1, 
					 first.denominator);
}

void main()
{
//	float a;
	Fraction a, b(3,4);

//	a = b.cvToFloat() + 1234;
	
	a = b + 1234;
	
}                      


					 

	