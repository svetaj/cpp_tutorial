// An overloaded function 

#include <iostream.h>
#include <string.h>

static char stringa[20], stringb[20];

inline void string_copy (char *dest, const char *src);
inline void string_copy (char *dest, const char *src, int len = 10);
                                                        // default value

void main()
{
        string_copy (stringa, "That");    // Error : ambigous
	string_copy (stringb, "This is a string", 4);
	cout << stringb << " and " << stringa;
}

inline void string_copy (char *dest, const char *src)
{
	strcpy (dest, src);
}

inline void string_copy (char *dest, const char *src, int len)
{
	strncpy (dest, src, len);
}


