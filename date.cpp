// DATE.CPP

#include <iostream.h>
#include <string.h>
#include "date.h"


void Date::setMonth (int mn)
{
	month = max (1, mn);
	month = min (month, 12);
}

void Date::setDay (int dy)
{
	static int length[] = { 0, 31, 28, 31, 30, 31, 30,
                                   31, 31, 30, 31, 30, 31 };
	                           
        day = max( 1, dy );
        day = min( day, length[month] );
}

void Date::setYear( int yr )
{
        year = max( 1, yr );
}

// -------- The constructor
Date::Date( int mn, int dy, int yr )
{
        setMonth( mn );
        setDay( dy );
        setYear( yr );
}

Date::Date()
{
	month = day = year = 1;
}
		
// -------- The destructor 
Date::~Date()
{
	// do nothing
}

// --------- Member function to print date
void Date::display()
{
	static char *name[] = 
	{
		"zero", "January", "Febryary", "March", "April", "May",
		"June", "July", "August", "September", "October",
		"November", "December"
	};
		
        cout << name[month] << ' ' << day << ", " << year << '\n';
}

