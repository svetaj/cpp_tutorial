// DATE.H
#if !defined( _DATE_H_ )

#define _DATE_H_

class Date
{
public: 
        Date ();                        // Constructor with no parameters
	Date (int mn, int dy, int yr);	// Constructor
                                        // Member functions:
        int  getMonth();                //    Get month
        int  getDay();                  //    Get day
        int  getYear();                 //    Get year
        void setMonth( int mn );        //    Set month
        void setDay( int dy );          //    Set day
        void setYear( int yr );         //    Set year
        void display();                 //    Print date
        ~Date();                        // Destructor
private:
        int month, day, year;           // Private data members
};


inline int Date::getMonth()
{
	return month;
}

inline int Date::getDay()
{
	return day;
}

inline int Date::getYear()
{
	return year;
}

// some useful functions
inline int max(int a, int b)
{
	if (a > b) return a;
	return b;
}

inline int min(int a, int b)
{
	if (a < b) return a;
	return b;
}

#endif      // _DATE_H_
