// Reference parameters for reducing overhead 
// and eliminating pointer notation

#include <iostream.h>
#include <string.h>

// ---------- A big structure
struct bigone
{
	int serno;
	char text[1000]; // A lot of chars
} bo = { 123, "This is a BIG structure"};


// ---- Three functions that have the structure as a parameter
void valfunc (bigone vl);   		// Call by value
void ptrfunc (const bigone *pl); 	// Call by pointer  
void reffunc (const bigone &rl);	// Call by reference
void valfunc1 (bigone vl);         // Call by value
void ptrfunc1 (bigone *pl);        // Call by pointer  
void reffunc1 (bigone &rl);        // Call by reference

void main()
{                                                       
        valfunc1 (bo);   
	valfunc (bo);	// Passing the variable itself
        ptrfunc1 (&bo);  
	ptrfunc (&bo);	// Passing the address of the variable
        reffunc1 (bo);   
	reffunc (bo);	// Passing a reference to the variable
}

    
// ------- Pass by value
void valfunc (bigone vl)
{
	cout << '\n' << vl.serno;
	cout << '\n' << vl.text;
}


// ------- Pass by pointer
void ptrfunc (const bigone *pl)
{
	cout << '\n' << pl->serno;   // Pointer notation
	cout << '\n' << pl->text;
}


// ------- Pass by reference
void reffunc (const bigone &rl)
{
	cout << '\n' << rl.serno;   // Reference notation
	cout << '\n' << rl.text;
}

    
// ------- Pass by value
void valfunc1 (bigone vl)
{
        vl.serno++;
        strcat(vl.text, "AAA");
}


// ------- Pass by pointer
void ptrfunc1 (bigone *pl)
{
        pl->serno++;
        strcat(pl->text, "AAA");
}


// ------- Pass by reference
void reffunc1 (bigone &rl)
{
        rl.serno++;
        strcat(rl.text, "AAA");
}



