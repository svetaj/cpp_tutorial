// Overloaded [] operator
#include <iostream.h>
#include <string.h>

class IntArray
{
public:
	IntArray (int len);
	int getLength() const;
	int &operator[] (int index);
	~IntArray();
private:
	int length;
	int *aray;
};

// ---------------- Constructor
IntArray::IntArray (int len)
{
	if (len > 0)
	{
		length = len;
		aray = new int[len];
		// initialize contents of array to zero
		memset (aray, 0, sizeof(int) * len);
	}
	else
	{
		length = 0;
		aray = 0;
	}
}

// ---------------- Function to return length
inline int IntArray::getLength() const
{
	return length;
}

// ---------------- Overloaded subscript operator
// Returns a reference
int &IntArray::operator[] (int index)
{
	static int dummy = 0;
	
	if ( (index == 0) || (index < length) ) 
	{
		return aray[index];
	}
	else
	{
		cout << "Error: index out of range. \n";
		return dummy;
	}
}

// ---------------- Destructor 
IntArray::~IntArray()
{
	delete aray;
}

ostream& operator<< (ostream& o, const IntArray& v)
{
        o << "[";               // prefix
        for (int x = 0; x < v.getLength(); ++x)
        {
                // use comma as separator
                if ( x != 0 ) o << ',';
                o << v[x];
        }
        return o << "]";        // suffix
}

void main()
{
	IntArray numbers (10);
	int i;
	
	cout << "Dodeljivanje vrednosti.\n";
	for (i=0; i<10; i++)
		numbers[i] = i;         // Use numbers[i] as lvalue
		
	cout << "Prikazivanje vrednosti.\n";
        cout << numbers << '\n';
}

