
#include <iostream.h>

class MyClass
{
public:
	MyClass();
	MyClass(int x);
	void printSecret() { cout << "topSecret = " << topSecret << '\n';}
	~MyClass();
private:
	friend class YourClass;
	int topSecret;
};

MyClass::MyClass()
{
	topSecret = 0;
}

MyClass::MyClass(int x)
{
	topSecret = x;
}

MyClass::~MyClass()
{
	;
}

class YourClass
{
public:           
	YourClass();
	YourClass(int x);
	void change (MyClass &yc); 
	~YourClass();
private:
	int newSecret;
};

YourClass::YourClass()
{
	newSecret = 0;
}

YourClass::YourClass(int x)
{
	newSecret = x;
}

YourClass::~YourClass()
{
	;
}

void YourClass::change (MyClass &yc)
{
	yc.topSecret = newSecret;
}


main()
{
	MyClass trt(50);
	YourClass mrt(100);
	
	trt.printSecret();	
	mrt.change(trt);
	trt.printSecret();	   
	return 0;
}


