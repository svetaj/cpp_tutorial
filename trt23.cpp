// Linkage specifications
#include <iostream.h>

extern "C"
{                    // The linkage specification
#include "mylib.h"   // tells C++ that mylib functions
}                    // were compiled with C

void main()
{
     cout << myfunc();
}
