// Free store exhaustion and the _set_new_handler function

#include <iostream.h>
#include <stdlib.h>
#include <new.h>

int all_gone (size_t size)
{
	cerr << "\n\aThe free store is empty\n";
	exit(1);
	return 0;
}


void main()
{
	_set_new_handler (all_gone);
	long total = 0;
	while (1)
	{
		char *gobble = new char[10000];
		total += 10000;
		cout << "Got 10000 for a total of " << total << '\n';
	}
}

