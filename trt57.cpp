// const objects and member functions

#include <iostream.h>
#include <string.h>

class Date
{
public: 
	Date (int mn, int dy, int yr);	// Constructor
                                        // Member functions:
        int  getMonth() const;          //    Get month - read-only
        int  getDay() const;            //    Get day - read-only
        int  getYear() const;           //    Get year - read-only
        void setMonth( int mn );        //    Set month
        void setDay( int dy );          //    Set day
        void setYear( int yr );         //    Set year
        void display() const;           //    Print date - read-only
        ~Date();                        // Destructor
private:
        int month, day, year;           // Private data members
};


inline int Date::getMonth() const
{
	return month;
}

inline int Date::getDay() const
{
	return day;
}

inline int Date::getYear() const
{
	return year;
}

// some useful functions
inline int max(int a, int b)
{
	if (a > b) return a;
	return b;
}

inline int min(int a, int b)
{
	if (a < b) return a;
	return b;
}

void Date::setMonth (int mn)
{
	month = max (1, mn);
	month = min (month, 12);
}

void Date::setDay (int dy)
{
	static int length[] = { 0, 31, 28, 31, 30, 31, 30,
                                   31, 31, 30, 31, 30, 31 };
	                           
        day = max( 1, dy );
        day = min( day, length[month] );
}

void Date::setYear( int yr )
{
        year = max( 1, yr );
}

// -------- The constructor
Date::Date( int mn, int dy, int yr )
{
	static int length[] = {0, 31, 28, 31, 30, 31, 30,
                                  31, 31, 30, 31, 30, 31};
	// Ignore leap years for simplicity
        month = max( 1, mn );
        month = min( month, 12 );
	
        day = max( 1, dy );
        day = min( day, length[month] );
	
        year = max( 1, yr );
}		

// -------- The destructor 
Date::~Date()
{
	// do nothing
}

// --------- Member function to print date
void Date::display() const
{
	static char *name[] = 
	{
		"zero", "January", "Febryary", "March", "April", "May",
		"June", "July", "August", "September", "October",
		"November", "December"
	};
		
        cout << name[month] << ' ' << day << ", " << year << '\n';
}


void main()
{
	int i;          
        const Date birthday( 7, 4, 1776 );
        birthday.display();
	
        i = birthday.getYear();         // Legal
        cout << "Year = " << i << '\n';
        birthday.setYear( 1492 );       // Error: setYear not cost
        birthday.display();             // BORLANDC++ (warning)
}
