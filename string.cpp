#include <iostream.h>
#include <string.h>
#include "string.h"

// Default constructor
String::String()
{
	buf[0] = 'A';
	buf[1] = 'B';
	buf[2] = 'C';
	buf[3] = '\0';
	length = 3;
}

// ----------- Constructor that takes a const char *
String::String (const char *s)
{
	length = strlen (s);
	buf = new char [length + 1];
	strcpy (buf, s);
}


// ----------- Constructor that takes a char and an int
String::String (char c, int n)
{
	length = n;
	buf = new char [length + 1];
	memset (buf, c, length);
	buf[length] = '\0';
}
                                        
// ----------- Assignment operator
String &String::operator=( const String &other)
{
	if (&other == this)
		return *this;
	length = other.length;
	delete buf;
	buf = new char[length + 1];
	strcpy (buf, other.buf); 
	return *this;
}
           
// ----------- Copy constructor
String::String (const String &other)
{
	length = other.length;
	buf = new char[length + 1];
	strcpy (buf, other.buf);
}
                             
// ----------- Set a character in a String
void String::set (int index, char newchar)
{
	if ((index > 0) && (index <= length))
		buf [index - 1] = newchar;
}

// ----------- Get a character in a String
char String::get (int index) const
{
	if ((index > 0) && (index <= length))
		return buf [index - 1];
	else
		return 0;
}
                                     
void String::append (const char *addition)
{
	char *temp;
	
	length += strlen (addition);
        temp = new char[length +1];     // Allocate new buffer
        strcpy (temp, buf);             // Copy contents of old buffer
	strcat (temp, addition);      	// Append new string
        delete buf;                     // Deallocate pld buffer
	buf = temp;
}

                 
// ----------- Destructor for a String
String::~String()
{
	delete buf;		// Works even for empty String; delete 0 is safe
}

	
