// Overloading the + operator
#include <stdlib.h>
#include <math.h>
#include <iostream.h>

class Fraction
{
public:
	Fraction();
	Fraction (long num, long den);
	void display() const;       
	operator float() const;
	friend Fraction operator+ (const Fraction &first, 
							   const Fraction &second);
private:
	long numerator, denominator;
};

// ------------- Default constructor
Fraction::Fraction()
{
	numerator = 0;
	denominator = 1;
}

// ------------- Constructor
Fraction::Fraction (long num, long den)
{
	if (den == 0)
		den = 1;
	numerator = num;
	denominator = den;
	if (den < 0)
	{
		numerator = -numerator;
		denominator = -denominator;
	}
}

// ------------- Function to print a Fraction
void Fraction::display() const
{
	cout << numerator << '/' << denominator; 
}

Fraction::operator float() const
{
	return (float) numerator / (float) denominator;
}

// ------------- Overloaded + operator
Fraction operator+ (const Fraction &first, const Fraction &second)
{
	long mult1, mult2;
	
	mult1 = first.denominator;
	mult2 = second.denominator;
	
	return Fraction (first.numerator * mult2 + second.numerator * mult1, 
					 first.denominator * second.denominator);
}


void main()
{
	Fraction a, b(3,4);
	
//	a = b + 1234;				// Error: ambiguous
								// 		a = (float)b + 1234;
								//		a = b + Fraction (1234);

	a = b + Fraction (1234, 1);
	
	a = Fraction (b + 1234, 1);
}                      


					 

	