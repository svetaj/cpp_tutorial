// Initialization of ANY const member

#include <iostream.h>

class Count
{
public:
        Count( int i );         // Constructor
        Count();                // Default constructor
        void display();
private:
        const int cnt;          // Constant integer member
};

Count::Count( int i )
        : cnt( i )      // Member initializer for integer
{
}

Count::Count()
        : cnt( -555 )      // Default member initializer for integer
{
}

void Count::display()
{
        cout << "cnt = " << cnt << '\n';
}

main()
{
        Count j( 999 );
        j.display();

        Count k;
        k.display();
}
