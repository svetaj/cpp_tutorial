// The date class
#include <iostream.h>

// ---------------- a Date class
class Date
{
public:
	Date (int mn, int dy, int yr );	// Constructor
        void display();                 // Function to print date
        ~Date();                        // Destructor
private:
        int month, day, year;           // Private data member 
};

// some useful functions
inline int max( int a, int b )
{
        if ( a > b ) return a;
	return b;
}

inline int min( int a, int b )
{
        if ( a < b ) return a;
	return b;
}

// --------------------- The constructor
Date::Date( int mn, int dy, int yr )
{
        static int length[] = { 0, 31, 28, 31, 30, 31, 30,
                                   31, 31, 30, 31, 30, 31 };
	// Ignore leap years for simplicity
        month = max( 1, mn );
        month = min( month, 12 );
	
        day = max( 1, dy );
        day = min( day, length[month] );
	
        year = max( 1, yr );
}

// --------- Member function to print date
void Date::display()
{
	static char *name[] = 
	{
		"zero", "January", "Febryary", "March", "April", "May",
		"June", "July", "August", "September", "October",
		"November", "December"
	};
		
	cout << name[month] << ' ' << day << ", " << year;
}
		
// -------- The destructor 
Date::~Date()
{
	// do nothing
}

// =================== Pointers and references to class
void main()
{
        Date myDate( 3, 12, 1985 );      // Declare a Date
        myDate.display();
	cout << '\n';

        Date *datePtr = &myDate;         // Pointer to an object
        datePtr->display();
	cout << '\n';           

        Date &otherDate = myDate;        // Reference to an object
        otherDate.display();
	cout << '\n';           
}



    
