// enum as a data type, more of 
#include <iostream.h>

enum color { red, orange, yellow, green, blue, violet };
// Values: 0, 1 , 2, 3, 4, 5

enum day { sunday = 1, monday, tuesday, wednesday = 24, 
			thursday, friday, saturday};
// Values: 1, 2, 3, 24, 25, 26, 27

enum direction { north = 1, south, east = 1, west};
// Values: 1, 2, 1, 2                 

void main()
{
        color myColor;
        myColor = blue;

        day myDay;
        myDay = friday;

        direction myDir;
        myDir = west;

        cout << "color = " << myColor << " day = " << myDay
             << " direction = " << myDir;
}
