// Difference between pointer and reference

main()
{
     int actualint = 123;
     const int &otherint = actualint;   // Reference to constant int

     int &const otherint1 = actualint;  // Error, meaningless, all ref.
                                        // are constant by definition 
}
