// Initialization of ANY const member

#include <iostream.h>

class Count
{
public:
        Count( int i );         // Constructor
        void display();
private:
        const int cnt;          // Constant integer member
};

Count::Count( int i )
        : cnt( i )      // Member initializer for integer
{
}

void Count::display()
{
        cout << "cnt = " << cnt;
}

main()
{
        Count j( 999 );
        j.display();
}
