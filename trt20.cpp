// enum, conversion to int
#include <iostream.h>

enum color { red, orange, yellow, green, blue, violet };
// Values: 0, 1 , 2, 3, 4, 5

void main()
{
     color myFavorite, yourFavorite;
     int i;

     myFavorite = blue;
     i = myFavorite;            // Legal; i = 4
//     cout << "myFavorite = " << myFavorite;

//     yourFavorite = 5;          // Error: cannot convert
                                // from int to color (not on BORLANDC) !
//     cout << "\nyourFavorite = " << yourFavorite;

     yourFavorite = (color) 4;  // Legal
//     cout << "\nyourFavorite = " << yourFavorite;


     yourFavorite = (color) 10;  // Legal
//     cout << "\nyourFavorite = " << yourFavorite;

//     yourFavorite = 10;         // Error
//     cout << "\nyourFavorite = " << yourFavorite;

}
