// Class specific new and delete operators with constructor, destructor

#include <iostream.h>
#include <malloc.h>

const int MAXNAMES = 10;

class Name
{
public:
	Name () { cout << "\nName's constructor running"; }
	void *operator new (size_t size);
	void operator delete (void *ptr);
	~Name() { cout << "\nName's destructor running"; }	
private:
	char name[25];
};

// --------- Simple memory pool to handle fixed number of Names 
char pool[sizeof(Name)];

// --------- Overloaded new operator for the Name class

void *Name::operator new (size_t)
{
	cout << "\nName's new running";
	return pool;
}

// ---------- Overloaded delete operator for the Names class
void Name::operator delete (void *p)
{
	cout << "\nName's delete running";
}

void main()
{
	cout << "\nExecuting: nm = new Name";
	Name *nm = new Name;
	cout << "\nExecuting: delete nm";
	delete nm;
}











