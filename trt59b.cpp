// Const member objects + member initilizer

#include <iostream.h>
#include <string.h>

class Date
{
public: 
        Date ();                        // Constructor with no parameters
	Date (int mn, int dy, int yr);	// Constructor
                                        // Member functions:
        int  getMonth();                //    Get month
        int  getDay();                  //    Get day
        int  getYear();                 //    Get year
        void setMonth( int mn );        //    Set month
        void setDay( int dy );          //    Set day
        void setYear( int yr );         //    Set year
        void display();                 //    Print date
        ~Date();                        // Destructor
private:
        int month, day, year;           // Private data members
};


inline int Date::getMonth()
{
	return month;
}

inline int Date::getDay()
{
	return day;
}

inline int Date::getYear()
{
	return year;
}

// some useful functions
inline int max(int a, int b)
{
	if (a > b) return a;
	return b;
}

inline int min(int a, int b)
{
	if (a < b) return a;
	return b;
}

void Date::setMonth (int mn)
{
	month = max (1, mn);
	month = min (month, 12);
}

void Date::setDay (int dy)
{
	static int length[] = { 0, 31, 28, 31, 30, 31, 30,
                                   31, 31, 30, 31, 30, 31 };
	                           
        day = max( 1, dy );
        day = min( day, length[month] );
}

void Date::setYear( int yr )
{
        year = max( 1, yr );
}

// -------- The constructor
Date::Date( int mn, int dy, int yr )
{
        setMonth( mn );
        setDay( dy );
        setYear( yr );
}

Date::Date()
{
	month = day = year = 1;
}
		
// -------- The destructor 
Date::~Date()
{
	// do nothing
}


// --------- Member function to print date
void Date::display()
{
	static char *name[] = 
	{
		"zero", "January", "Febryary", "March", "April", "May",
		"June", "July", "August", "September", "October",
		"November", "December"
	};
		
        cout << name[month] << ' '
             << day << ", "
             << year << '\n';
}


class PersonInfo
{
public:
        PersonInfo( char *nm, char*addr, int mn, int dy, int yr);
        void showinfo();
private:
        char name[30];
        char address[60];
        const Date birthday;          // member object
};


PersonInfo::PersonInfo( char *nm, char *addr, int mn, int dy, int yr )
        : birthday( mn, dy, yr )        // Member initializer
{
        strncpy( name, nm, 30 );
        strncpy( address, addr, 60 );
}


// --------- Member function to print date
void PersonInfo::showinfo()
{
        cout << "NAME = " << name << '\n';
        cout << "ADDRESS = " << address << '\n';
        cout << "BIRTHDAY = ";
        birthday.display();
}


void main()
{ 
        PersonInfo artur("Artur", "Djordja Kratovca 9", 13, 3, 1995);

        artur.showinfo();
}
