
#include <iostream.h>

class MyClass
{
friend void change (MyClass &yc);
public:
	MyClass();
	MyClass(int x);
	void printSecret() { cout << "topSecret = " << topSecret << '\n';}
	~MyClass();
private:
	int topSecret;
};

MyClass::MyClass()
{
	topSecret = 0;
}

MyClass::MyClass(int x)
{
	topSecret = x;
}

MyClass::~MyClass()
{
	;
}

class YourClass
{
public:           
	YourClass();
	YourClass(int x);
	void change (MyClass &yc); 
	~YourClass();
private:
	int newSecret;
};

YourClass::YourClass()
{
	newSecret = 0;
}

YourClass::YourClass(int x)
{
	newSecret = x;
}

YourClass::~YourClass()
{
	;
}

void change (MyClass &yc)
{
	yc.topSecret++;
}


main()
{
	MyClass trt(50);
	YourClass mrt(100);
	
	trt.printSecret();	
	change(trt);
	trt.printSecret();	   
	return 0;
}


