#include <iostream.h>
#include <string.h>

class String
{
public: 
	String();
	String (const char *s);
	String (char c, int n);        
	String (const String &other);				// Copy constructor
	void set (int index, char newchar);
	char get (int index) const;
	int getLength() const {return length;}
	void display() const {cout << buf << '\n';}    
	void append (const char *addition);
	String &operator=(const String &other);
	~String();
private:
	int length;
	char *buf;
};

