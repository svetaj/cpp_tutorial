#include <iostream.h>
#include <string.h>

#define MAXLIST 100

class SortableName
{
public:  
	SortableName() { strncpy(name, "sveta", 5); }
	SortableName(char *nm) { strncpy(name, nm, 30);}
	void printName() { cout << "name = " << name <<'\n';}
	int isEqual (const SortableName &other) const;
	int isLessThan (const SortableName &other) const;
private:
	char name[30];
};

int SortableName::isEqual (const SortableName &other) const
{
	return (strncmp (name, other.name, 30) == 0);
};

int SortableName::isLessThan (const SortableName &other) const
{
	return (strncmp (name, other.name, 30) < 0);
};
   
class SortedList
{
public:
	SortedList();
	void addItem (const SortableName &newItem);
	void printSorted();
private:
	int firstEmpty;
	SortableName list[MAXLIST];
};

SortedList::SortedList()
{
	firstEmpty = 0;
}                               

void SortedList::addItem (const SortableName &newItem)
{
	if (firstEmpty == 0)
	{
		list[0] = newItem;
		firstEmpty++;
	}
	else                      
	{
		int firstOld;		
		firstOld = firstEmpty;		
		for (int i=0; i < firstEmpty; i++) 
		{
			if (newItem.isLessThan(list[i])) {
				 for (int j=firstEmpty; j>i; j--)
				 	list[j] = list[j-1];
				 list[i] = newItem;
				 firstEmpty++;
				 break;
			}
		}
		if ( firstOld == firstEmpty) 
		{
			list[firstEmpty] = newItem;
			firstEmpty++;
		}	
	}
}			  

void SortedList::printSorted()
{
	for (int i = 0; i<firstEmpty; i++) 
		list[i].printName();      
}
	
   
main()
{ 
	SortedList xList;
	              
	cout << "Pocetak \n";
	SortableName imena[10] = {"sisoje", "pera",  "mika",  "alempije",
                                   "kukoje", "mlakoje", "prtoje", "avram",
                                   "slobo", "jeboslava"};

	cout << "############# Ne sortirana lista #############\n";	
	for (int i=0; i<10; i++)
		imena[i].printName();
	
	for (i=0; i<10; i++)
		xList.addItem (imena[i]);

	cout << "@@@@@@@@@@@@@@@ Sortirana lista @@@@@@@@@@@@@@\n";	
	xList.printSorted();    
	cout << "Kraj \n";
	
	return 0;
}

 
