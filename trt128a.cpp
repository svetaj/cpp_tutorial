#include <iostream.h>
#include <string.h>

#define MAXLIST 100

class SortableObject
{
public:
	virtual int isEqual (const SortableObject &other) const = 0;
	virtual int isLessThan (const SortableObject &other) const = 0;
	virtual void printName() = 0;
};

class SortableName : public SortableObject
{
public:  
	SortableName() { strncpy(name, "sveta", 5); }
	SortableName(char *nm) { strncpy(name, nm, 30);}
	void printName() { cout << "name = " << name <<'\n';}
	int isEqual (const SortableObject &other) const;
	int isLessThan (const SortableObject &other) const;
private:
	char name[30];
};

int SortableName::isEqual (const SortableObject &other) const
{
	return (strncmp (name, other.name, 30) == 0);
};

int SortableName::isLessThan (const SortableObject &other) const
{
	return (strncmp (name, other.name, 30) < 0);
};
   
class SortedList
{
public:
	SortedList();
	void addItem (const SortableObject &newItem);
	void printSorted();
private:
	int firstEmpty;
	SortableObject list[MAXLIST];
};

SortedList::SortedList()
{
	firstEmpty = 0;
}                               

void SortedList::addItem (const SortableObject &newItem)
{
	if (firstEmpty == 0)
	{
		cout << "addItem pocetak\n";
		list[0] = newItem;
		firstEmpty++;
	}
	else                      
	{
		cout << "addItem posle\n";
		for (int i=0; i < firstEmpty; i++) 
		{
			if (newItem.isLessThan(list[i])) {
				 for (int j=firstEmpty; j>i; j--)
				 	list[j] = list[j--];
				 list[i] = newItem;
				 break;
			}
		}	
		cout << "firstEmpty = " << firstEmpty << '\n';
	}
}			  

void SortedList::printSorted()
{
	for (int i = 0; i<firstEmpty; i++) 
		list[i].printName();      
}
	
   
main()
{ 
	SortedList xList;
	              
	cout << "Pocetak \n";
	SortableName imena[10] = {"sisoje", "pera",  "mika",  "alempije",
						      "prtoje", "avram", "slobo", "jeboslava"};
	
	for (int i=0; i<10; i++)
		xList.addItem (imena[i]);

	xList.printSorted();    
	cout << "Kraj \n";
	
	return 0;
}

 
