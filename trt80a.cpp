#include "string.h"

// Function that returns a String
String emit()
{
        String retValue( "here's a return value" );

        return retValue;
}

void main()
{
        String yourString;

        yourString = emit();         // String temp( retValue );
                                     // yourString = temp;
        yourString.display();
}
