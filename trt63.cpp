//  The heap + malloc
#include <stdio.h>
#include <malloc.h>

struct date
{
        int month;
        int day;
        int year;
};

void display_date( struct date *dt );

main()
{
        struct date *dateptr;

        dateptr = (struct date *)malloc( sizeof( struct date ) );

        dateptr->month = 1;
        dateptr->day = 23;
        dateptr->year = 1985;

        display_date( dateptr );
}

void display_date( struct date *dt )
{
        static char *name[] =
        {
                "zero", "January", "February", "March", "April", "May",
                "June", "July", "August", "September", "October",
                "November", "December"
        };

        printf( "%s %d, %d", name[dt->month], dt->day, dt->year );
}


