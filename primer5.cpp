template<class T> class vector
{
        T* v;
        int sz;
public:
        vector( int );
        T& operator[]( int );
        T& elem( int i ) { return v[i]; }
};

vector<int> v1(20);
vector<complex> v2(30);

typedef vector<complex> cvec;   // cvec ----> sinomim za vector<complex>
cvec v3(40);                    // v2 i v3 su istog tipa

v1[3] = 7;
v2[3] = v3.elem(4) = complex(7,8);


