/* Example of implementing related types in C */

struct wage_pay
{
	float wage;
	float hrs;
};

struct sales_pay
{
	float wage;
	float hrs;
	float commision;
	float sales_made;
};

struct mgr_pay
{
	float weekly_salary;
};

enum { WAGE_EMPLOYEE, SALESPERSON, MANAGER} EMPLOYEE_TYPE;

struct employee
{
	char name[30];
	EMPLOYEE_TYPE type;
	union
	{
		struct wage_pay worker;
		struct sales_pay seller;
		struct mgr_pay mgr;
	};								// Anonymous union
};

float compute_pay (struct employee *emp)
{
	switch (emp->type)
	{
	case WAGE_EMPLOYEE:
		return emp->worker.hrs * emp->worker.wage;
		break;
	case SALESPERSON:
		return 	emp->seller.hrs * emp->seller.wage +
				emp->seller.commissions * emp->seller.sales_made;
		break;
	case MANAGER:
		return emp->mgr.weekly_salary;
		break;
	};
}
