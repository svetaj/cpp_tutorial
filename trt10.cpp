// A program with a function prototype
#include <iostream.h>

void display (char *s);

void main()
{
        display("Hello, world");
}

void display (char *s)
{
        cout << s;
}

